#!/bin/bash

set -eux
set -o pipefail

cpus="$(nproc)"
top="$(pwd)"
reldir="${top}/public"
use_cpus=$((cpus / 2))
[[ "${use_cpus}" -eq 0 ]] && use_cpus=1

if [ -z "${MAKEFLAGS:-}" ] ; then
  MAKEFLAGS="-j${use_cpus}"
fi

mkdir -p "${reldir}"

# get ipxe dist
git clone -q https://git.ipxe.org/ipxe.git

# copy configs in to ipxe tree
cp -R src ipxe

# make a git bundle, patch
pushd ipxe
gv=$(git rev-parse --short HEAD)
git bundle create "${reldir}/ipxe.gitbundle" master
if [ "$(egrep -q '\[(BUILD|DEPS)\]' src/Makefile.housekeeping ; echo $?)" != 1 ] ; then
  patch -p1 < ../Makefile.housekeeping.patch
fi
pushd src

# build configs
for cfg in "${top}/src/config"/* ; do
 b=${cfg##*/}
 case ${b} in
  packer) embed="EMBED=packer.ipxe" ;;
  com*)   embed="EMBED=${b}.ipxe" ;;
  *)      embed="" ;;
 esac
 targs=("bin/ipxe.pxe" "bin/ipxe.lkrn" "bin-x86_64-efi/ipxe.efi" "bin-i386-efi/ipxe.efi" "bin/ipxe.iso")
 # shellcheck disable=SC2086
 make ${MAKEFLAGS} NO_WERROR=1 V=0 GITVERSION="${gv}" CROSS_COMPILE=x86_64-linux-gnu- CONFIG="${b}" "${targs[@]}" ${embed}
 mkdir -p "${reldir}/${b}"
 mv bin/ipxe.lkrn "${reldir}/${b}/ipxe-pcbios.lkrn"
 mv bin/ipxe.pxe "${reldir}/${b}/ipxe-pcbios.pxe"
 mv bin-x86_64-efi/ipxe.efi "${reldir}/${b}/ipxe-x86_64.efi"
 mv bin-i386-efi/ipxe.efi "${reldir}/${b}/ipxe-i386.efi"
 mv bin/ipxe.iso "${reldir}/${b}/ipxe.iso"
 make clean
done

# get out of ipxe/src now
popd && popd

rm -rf ipxe

# clean any old SHA256SUMS
rm -rf "${reldir}/SHA256SUMS"

# record version for release
echo "${gv}" > "${reldir}/IPXE_VERSION"

# bundle files
(cd "${reldir}" && find . -type f ! -iname \*.iso ! -iname \*.gitbundle ! -iname \*.tar | xargs tar cf "bin.tar")

# remove bundled files
find "${reldir}" -type f ! -iname \*.iso ! -iname \*.gitbundle ! -iname \*.tar | xargs rm

# compress files
for f in "${reldir}"/* ; do
  [[ ! -f "${f}" ]] && continue
  pxz "${f}"
done

sha256out="$(mktemp)"
# sha256sum files
(cd "${reldir}" && find * -type f -exec sha256sum {} \;) > "${sha256out}"
mv "${sha256out}" "${reldir}/SHA256SUMS"
